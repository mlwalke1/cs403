package edu.svsu.zodiac;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class TopLevelActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_level);
        String[] signs = getSigns();
        ArrayAdapter listAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1, signs);
        ListView listSigns = (ListView) findViewById(R.id.list_options);
        listSigns.setAdapter(listAdapter);

        //Create the listener
        AdapterView.OnItemClickListener itemClickListener =
                new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> listSigns,
                                            View itemView,
                                            int position,
                                            long id) {
                        //Pass the sign the user clicks on to SignActivity
                        Intent intent = new Intent(TopLevelActivity.this,
                                SignActivity.class);
                        intent.putExtra(SignActivity.EXTRA_SIGNID, (int) id);
                        startActivity(intent);
                    }
                };
        //Assign the listener to the list view
        listSigns.setOnItemClickListener(itemClickListener);
    }

    public String[] getSigns() {
        SQLiteOpenHelper zodiacDatabaseHelper = new ZodiacDatabaseHelper(this);
        ArrayList<String> names = new ArrayList<>();
        try {
            Cursor cursor = zodiacDatabaseHelper.getReadableDatabase().rawQuery("SELECT NAME FROM SIGNS", null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                names.add(cursor.getString(cursor.getColumnIndex("NAME")));
                cursor.moveToNext();
            }
            cursor.close();
        }  catch(SQLiteException e) {
            Toast toast = Toast.makeText(this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
        return names.toArray(new String[names.size()]);
    }
}