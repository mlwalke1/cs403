package edu.svsu.zodiac;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

//import android.widget.ImageView;

public class SignActivity extends Activity {
    public static final String EXTRA_SIGNID = "signId";
    private DailyHoroscope zodiacApi = new DailyHoroscope();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);

        //Get the sign from the intent
        int signId = (Integer)getIntent().getExtras().get(EXTRA_SIGNID);

        //Create a cursor
        SQLiteOpenHelper zodiacDatabaseHelper = new ZodiacDatabaseHelper(this);
        try {
            SQLiteDatabase db = zodiacDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.query ("SIGNS",
                    new String[] {"NAME", "DESCRIPTION", "SYMBOL", "MONTH"},
                    "_id = ?",
                    new String[] {Integer.toString(signId + 1)},
                    null, null,null);
            //Move to the first record in the Cursor
            if (cursor.moveToFirst()) {
                //Get the drink details from the cursor
                String nameText = cursor.getString(0);
                String descriptionText = cursor.getString(1);
                String symbolText = cursor.getString(2);
                String monthText = cursor.getString(3);

                //Populate the sign name
                TextView name = (TextView)findViewById(R.id.name);
                name.setText(nameText);

                //Populate the sign description
                TextView description = (TextView)findViewById(R.id.description);
                description.setText(descriptionText);

                //Populate the sign symbol
                TextView symbol = (TextView)findViewById(R.id.symbol);
                symbol.setText(symbolText);

                //Populate the month
                TextView month = (TextView)findViewById(R.id.month);
                month.setText(monthText);

                //Populate the daily horoscope
                TextView horoscope = (TextView) findViewById(R.id.daily_horoscope);
                String dailyZodiac = zodiacApi.getDailyHoroscope(nameText);
                horoscope.setText(dailyZodiac);



            }
        } catch(SQLiteException e) {
            Toast toast = Toast.makeText(this, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}