package edu.svsu.roshambo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.ArrayMap;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    ImageView player,computer;
    Button rock,paper,scissors;
    String player_choice;
    String computer_choice;
    Random rand;
    String result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        player = findViewById(R.id.player);
        computer = findViewById(R.id.computer);

        rock = findViewById(R.id.rock);
        paper = findViewById(R.id.paper);
        scissors = findViewById(R.id.scissors);

        rand = new Random();

        rock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                player_choice = "rock";
                player.setImageResource(R.drawable.rock);
                compare();
            }
        });
        paper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                player_choice = "paper";
                player.setImageResource(R.drawable.paper);
                compare();
            }
        });
        scissors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                player_choice = "scissors";
                player.setImageResource(R.drawable.scissors);
                compare();
            }
        });

    }

    private void compare() {
        int comp = rand.nextInt(3);
        switch(comp) {
            case 1:
                computer_choice = "rock";
                computer.setImageResource(R.drawable.rock);
                break;
            case 2:
                computer_choice = "paper";
                computer.setImageResource(R.drawable.paper);
                break;
            case 3:
                computer_choice = "scissors";
                computer.setImageResource(R.drawable.scissors);
                break;
        }

        if(player_choice.equals(computer_choice)) {
            result = "draw";
        }
        else if(player_choice == "rock" && computer_choice == "scissors") {
            result = "win";
        }
        else if(player_choice == "rock" && computer_choice == "paper") {
            result = "lose";
        }
        else if(player_choice == "paper" && computer_choice == "rock") {
            result = "win";
        }
        else if(player_choice == "paper" && computer_choice == "scissors") {
            result = "lose";
        }
        else if(player_choice == "scissors" && computer_choice == "paper") {
            result = "win";
        }
        else if(player_choice == "scissors" && computer_choice == "rock") {
            result = "lose";
        }

        Toast.makeText(MainActivity.this,result,Toast.LENGTH_SHORT).show();
    }
}
