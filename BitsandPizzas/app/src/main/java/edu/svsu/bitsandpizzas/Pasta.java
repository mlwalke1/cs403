package edu.svsu.bitsandpizzas;

/**
 * Created by davidg on 04/05/2017.
 */

public class Pasta {
    private String name;
    private int imageResourceId;

    public static final Pasta[] pasta = {
            new Pasta("Spaghetti Bolognese", R.drawable.bolognese),
            new Pasta("Lasagna", R.drawable.lasagna)
    };

    private Pasta(String name, int imageResourceId) {
        this.name = name;
        this.imageResourceId = imageResourceId;
    }
    public String getName() {
        return name;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }
}