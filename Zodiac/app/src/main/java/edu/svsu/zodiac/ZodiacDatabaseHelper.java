package edu.svsu.zodiac;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

public class ZodiacDatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "signs"; // the name of our database
    private static final int DB_VERSION = 1; // the version of the database

    ZodiacDatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        updateMyDatabase(db, 0, DB_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        updateMyDatabase(db, oldVersion, newVersion);
    }

    private static void insertSign(SQLiteDatabase db, String name, String description,
                                    String symbol, String month) {
        ContentValues signValues = new ContentValues();
        signValues.put("NAME", name);
        signValues.put("DESCRIPTION", description);
        signValues.put("SYMBOL", symbol);
        signValues.put("MONTH", month);

        db.insert("SIGNS", null, signValues);
    }

    private void updateMyDatabase(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 1) {
            db.execSQL("CREATE TABLE SIGNS (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "NAME TEXT, "
                    + "DESCRIPTION TEXT, "
                    + "SYMBOL TEXT, "
                    + "MONTH TEXT);");
            insertSign(db, "Aries", "Courageous and Energetic", "Ram", "April");
            insertSign(db, "Taurus", "Known for being reliable, practical, ambitious and sensual",
                    "Bull", "May");
            insertSign(db, "Gemini", "Gemini-born are clever and intellectual", "Twins", "June");
            insertSign(db, "Cancer", "Tenacious, loyal and sympathetic", "Crab", "July");
            insertSign(db, "Leo", "Warm, action-oriented and driven by the desire to be loved and " +
                    "admired.", "Lion", "August");
            insertSign(db, "Virgo", "Methodical, meticulous, analytical and mentally astute.",
                    "Virgin","September");
            insertSign(db, "Libra", "Librans are famous for maintaining balance and harmony.",
                    "Scales","October");
            insertSign(db, "Scorpio", "Strong willed and mysterious.", "Scorpion","November");
            insertSign(db, "Sagittarius", "Born adventurers.", "Archer","December");
            insertSign(db, "Capricorn", "The most determined sign in the Zodiac.", "Goat","January");
            insertSign(db, "Aquarius", "Humanitarians to the core.", "Water Bearer","February");
            insertSign(db, "Pisces", "Proverbial dreamers of the Zodiac.", "Fish","March");
        }
        if (oldVersion < 2) {
            // Upgrade
        }
    }
}

