package edu.svsu.breadgenius;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class InsertActivity extends AppCompatActivity {
    BreadDatabaseHelper myDb;
    EditText editDesc, editAmt, editMonth, editType;
    Button btnInsert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);
        myDb = new BreadDatabaseHelper(this);
        editDesc = findViewById(R.id.description);
        editAmt = findViewById(R.id.amount);
        editMonth = findViewById(R.id.month);
        editType = findViewById(R.id.type);
        btnInsert = findViewById(R.id.btnInsert);
    }

    public void insertDB(View view){
        String description = editDesc.getText().toString();
        double amount = Double.parseDouble(editAmt.getText().toString());
        int month = Integer.parseInt(editMonth.getText().toString());
        String type = editType.getText().toString();
        boolean inserted = myDb.insertData(description, amount, month, type);
        if(inserted) {
            Toast.makeText(this,"Data Inserted.",Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(this,"Error Inserting.",Toast.LENGTH_SHORT).show();
        }
    }

    public void btnBack(View view) {
        Intent intent = new Intent(this,AccountActivity.class);
        startActivity(intent);
    }
}
