package edu.svsu.breadgenius;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BreadDatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "breadgenious.db"; // the name of our database
    public static final String TABLE_NAME = "bg_transaction";
    public static final String COL_1 = "description";
    public static final String COL_2 = "amt";
    public static final String COL_3 = "month";
    public static final String COL_4 = "class";

    BreadDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "description TEXT, "
                + "amt REAL, "
                + "month INTEGER, "
                + "class TEXT);");
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //upgrade
    }

    public boolean insertData(String description, double amount, int month, String app_class) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1,description);
        contentValues.put(COL_2,amount);
        contentValues.put(COL_3,month);
        contentValues.put(COL_4,app_class);
        long result = db.insert(TABLE_NAME,null,contentValues);
        if(result == -1)
            return false;
        else return true;
    }

    public Cursor retrieveData(int month) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from bg_transaction where month =" + month,null);
        return cursor;
    }

}
