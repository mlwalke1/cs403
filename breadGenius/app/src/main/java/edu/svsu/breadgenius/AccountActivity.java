package edu.svsu.breadgenius;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AccountActivity extends AppCompatActivity {
    private ExpandableListView listView, listView2, listView3;
    private android.widget.ExpandableListAdapter listAdapter, listAdapter2, listAdapter3;
    private List<String> listDataHeader, listDataHeader2, listDataHeader3;
    private List<String> r_earn, nr_earn, e_earn, r_spend, nr_spend, e_spend;
    private HashMap<String,List<String>> listHash, listHash2, listHash3;
    BreadDatabaseHelper myDb;
    double e_total, s_total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        listView = findViewById(R.id.lvExpand);
        listView2 = findViewById(R.id.lvExpand2);
        listView3 = findViewById(R.id.lvExpand3);
        initData();
        listAdapter = new ExpandableListAdapter(this,listDataHeader,listHash);
        listAdapter2 = new ExpandableListAdapter(this,listDataHeader2,listHash2);
        listAdapter3 = new ExpandableListAdapter(this,listDataHeader3,listHash3);
        listView.setAdapter(listAdapter);
        listView2.setAdapter(listAdapter2);
        listView3.setAdapter(listAdapter3);

    }

    private void initData() {
        // Spending
        listDataHeader = new ArrayList<>();
        listHash = new HashMap<>();
        getData();
        listDataHeader.add("Regular Spending");
        listDataHeader.add("Non-Regular Spending");
        listDataHeader.add("Extra Spending");

        listHash.put(listDataHeader.get(0),r_spend);
        listHash.put(listDataHeader.get(1),nr_spend);
        listHash.put(listDataHeader.get(2),e_spend);

        // Earning
        listDataHeader2 = new ArrayList<>();
        listHash2 = new HashMap<>();

        listDataHeader2.add("Regular Earning");
        listDataHeader2.add("Non-Regular Earning");
        listDataHeader2.add("Extra Earning");

        listHash2.put(listDataHeader2.get(0),r_earn);
        listHash2.put(listDataHeader2.get(1),nr_earn);
        listHash2.put(listDataHeader2.get(2),e_earn);

        // Total
        listDataHeader3 = new ArrayList<>();
        listHash3 = new HashMap<>();
        double previous = 2000.00;
        double monthlyTotal = e_total - s_total;
        double grandTotal = previous + monthlyTotal;
        String s_totalString = String.format("%.2f", s_total);
        String e_totalString = String.format("%.2f", e_total);
        String previousStr = String.format("%.2f", previous);
        String monthlyTotalStr = String.format("%.2f", monthlyTotal);
        String grandTotalStr = String.format("%.2f", grandTotal);

        TextView total = findViewById(R.id.total);
        total.setText("Total - $" + grandTotalStr);

        listDataHeader3.add("Monthly Total - $" + monthlyTotalStr);
        listDataHeader3.add("Previous Balance - $" + previousStr);

        List<String> totals = new ArrayList<>();
        totals.add("Total Spending - $" + s_totalString);
        totals.add("Total Earning - $" + e_totalString);

        List<String> balance = new ArrayList<>();
        balance.add("Previous Balance - $" + previousStr);

        listHash3.put(listDataHeader3.get(0),totals);
        listHash3.put(listDataHeader3.get(1),balance);

    }

    private void getData() {
        myDb = new BreadDatabaseHelper(this);
        r_spend = new ArrayList<>();
        nr_spend = new ArrayList<>();
        e_spend = new ArrayList<>();
        r_earn = new ArrayList<>();
        nr_earn = new ArrayList<>();
        e_earn = new ArrayList<>();
        e_total = 0.00;
        s_total = 0.00;

        Cursor cursor = myDb.retrieveData(12);
        if(cursor.getCount() == 0) {
            Toast.makeText(this,"No Data",Toast.LENGTH_SHORT).show();
        }
        else {
            while(cursor.moveToNext()){
                String type = cursor.getString(4);
                //Toast.makeText(this,type,Toast.LENGTH_SHORT).show();
                switch(type) {
                    case "r_spend":
                        r_spend.add(cursor.getString(1) + " - $" + String.format("%.2f",cursor.getDouble(2)));
                        s_total += cursor.getDouble(2);
                        break;
                    case "nr_spend":
                        nr_spend.add(cursor.getString(1) + " - $" + String.format("%.2f",cursor.getDouble(2)));
                        s_total += cursor.getDouble(2);
                        break;
                    case "e_spend":
                        e_spend.add(cursor.getString(1) + " - $" + String.format("%.2f",cursor.getDouble(2)));
                        s_total += cursor.getDouble(2);
                        break;
                    case "r_earn":
                        r_earn.add(cursor.getString(1) + " - $" + String.format("%.2f",cursor.getDouble(2)));
                        e_total += cursor.getDouble(2);
                        break;
                    case "nr_earn":
                        nr_earn.add(cursor.getString(1) + " - $" + String.format("%.2f",cursor.getDouble(2)));
                        e_total += cursor.getDouble(2);
                        break;
                    case "e_earn":
                        e_earn.add(cursor.getString(1) + " - $" + String.format("%.2f",cursor.getDouble(2)));
                        e_total += cursor.getDouble(2);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public void startInsert(View view) {
        Intent intent = new Intent(this,InsertActivity.class);
        startActivity(intent);
    }
}
